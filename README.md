# README #

The RESCUECIRCLE site displays the location of every short-haul helicopter currently available for Wildland Firefighter rescue.

### Versions & Dependencies ###

The site was built on the following web stack, although it may work just fine on future versions of PHP:

* PHP 5.3.4
* Apache 2.2.17
* MySQL 5.1
* jQuery 1.11.3
* Bootstrap 3.3.5

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact